# file_fetcher

The simplest library to get files over http or locally.
*DISCLAIMER: use reqwest to manage http, so this library is small but its dependencies... not so much*.

## Cargo.toml dependencies

```TOML
[dependencies]
file_fetcher = "0.1"
```

## Examples

To load a local file (here ./src/lib.rs)

```rust
use std::fs;
use file_fetcher::open_bytes_str;

let path = fs::canonicalize("./src/lib.rs").unwrap();
let local_bytes = open_bytes_str(&format!("file://{}", path.display()))
                  .unwrap();
```

To load a remote file

```rust
use file_fetcher::open_bytes_str;

let remote_bytes = open_bytes_str("https://gitlab.com/Artefaritaj/file-fetcher/raw/master/src/lib.rs")
                    .unwrap();
```
