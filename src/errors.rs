// @Author: Lashermes Ronan <ronan>
// @Date:   25-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT



 error_chain! {
     foreign_links {
        Fmt(::std::fmt::Error);
        Io(::std::io::Error);
        Reqwest(::reqwest::Error);
        Url(::reqwest::UrlError);
        SerdeJson(::serde_json::Error);
    }

    // errors {
    //     ProtocolNotSupported(protocol: String) {
    //         description("protocol not supported")
    //         display("protocol {} is not supported", protocol)
    //     }
    // }
 }
