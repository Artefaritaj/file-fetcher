// @Author: Lashermes Ronan <ronan>
// @Date:   25-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT

use std::path::Path;
use std::fs::File;
use std::io::{Read, BufReader};

use crate::errors::*;

use serde::de::DeserializeOwned;
use serde_json;

///Open a file on local disk, read it and give its bytes
pub fn open_bytes<T: AsRef<Path>>(path: T) -> Result<Vec<u8>> {
    let file = File::open(path)?;
    let mut reader = BufReader::new(file);
    let mut buffer: Vec<u8> = Vec::new();
    reader.read_to_end(&mut buffer)?;

    Ok(buffer)
}

///Open a file on local disk, give its reader
pub fn open<T: AsRef<Path>>(path: T) -> Result<Box<dyn Read>> {
    let file = Box::new(File::open(path)?);
    Ok(file)
}

///Read file and get json struct
pub fn open_json<T: DeserializeOwned, P: AsRef<Path>>(path: P) -> Result<T> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);
    Ok(serde_json::from_reader(reader)?)
}
