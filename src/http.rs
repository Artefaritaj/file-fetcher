// @Author: Lashermes Ronan <ronan>
// @Date:   25-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT


use reqwest;
// use url::Url;
use reqwest::Url;
use serde::de::DeserializeOwned;

use crate::errors::*;
use std::io::Read;

///Read url with http get and give resulting bytes
pub fn open_bytes(url: Url) -> Result<Vec<u8>> {
    let mut result = Vec::new();
    reqwest::get(url)?.read_to_end(&mut result)?;
    Ok(result)
}

///Read url with http get and give resulting reader
pub fn open(url: Url) -> Result<Box<dyn Read>> {
    Ok(Box::new(reqwest::get(url)?))
}

///Read url and get json struct
pub fn open_json<T: DeserializeOwned>(url: Url) -> Result<T> {
    Ok(reqwest::get(url)?.json::<T>()?)
}

#[test]
fn test_file_fetch() {
    assert!(reqwest::get("https://gitlab.com/Artefaritaj/file-fetcher/raw/master/src/http.rs").is_ok())
}
