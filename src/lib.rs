//! The 'file_fetcher' crate can get you file either locally or over http(s).

// @Author: Lashermes Ronan <ronan>
// @Date:   25-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT

// `error_chain!` can recurse deeply
#![recursion_limit = "1024"]

#[macro_use] extern crate error_chain;
extern crate reqwest;
extern crate serde;
extern crate serde_json;

pub mod errors;
pub mod local;
pub mod http;

use errors::*;
pub use reqwest::{Url, UrlError};
use serde::de::DeserializeOwned;

use std::io::Read;


/// Open a string url to get bytes
///
/// # Arguments
/// * 'url' - the url to open
///
/// # Examples
///
/// To load a local file (here ./src/lib.rs)
///
/// ```
/// use std::fs;
///
/// let path = fs::canonicalize("./src/lib.rs").unwrap();
/// let local_bytes = file_fetcher::open_bytes_str(&format!("file://{}", path.display())).unwrap();
/// ```
///
/// To load a remote file
///
/// ```
/// let remote_bytes = file_fetcher::open_bytes_str("https://gitlab.com/Artefaritaj/file-fetcher/raw/master/src/lib.rs").unwrap();
/// ```
pub fn open_bytes_str(url: &str) -> Result<Vec<u8>> {
    let url = Url::parse(url)?;
    open_bytes(url)
}

/// Open an url to get bytes
pub fn open_bytes(url: Url) -> Result<Vec<u8>> {
    let scheme = url.scheme().to_string();
    match scheme.as_str() {
        "http"|"https" => http::open_bytes(url),
        "file" => local::open_bytes(url.path()),
        prot => Err(format!("Protocol {} is not supported.", prot).into())
    }
}

///Open a string url to get a reader
///
/// # Arguments
/// * 'url' - the url to open
///
/// # Examples
///
/// To load a local file (here ./src/lib.rs)
///
/// ```
/// use std::fs;
///
/// let path = fs::canonicalize("./src/lib.rs").unwrap();
/// let local_reader = file_fetcher::open_str(&format!("file://{}", path.display())).unwrap();
/// ```
///
/// To load a remote file
///
/// ```
/// let remote_reader = file_fetcher::open_str("https://gitlab.com/Artefaritaj/file-fetcher/raw/master/src/lib.rs").unwrap();
/// ```
pub fn open_str(url: &str) -> Result<Box<dyn Read>> {
    let url = Url::parse(url)?;
    open(url)
}

/// Open an url to get a reader
pub fn open(url: Url) -> Result<Box<dyn Read>> {
    let scheme = url.scheme().to_string();
    match scheme.as_str() {
        "http"|"https" => http::open(url),
        "file" => local::open(url.path()),
        prot => Err(format!("Protocol {} is not supported.", prot).into())
    }
}

/// Open an url to get a json struct
pub fn open_json<T: DeserializeOwned>(url: Url) -> Result<T> {
    let scheme = url.scheme().to_string();
    match scheme.as_str() {
        "http"|"https" => http::open_json(url),
        "file" => local::open_json(url.path()),
        prot => Err(format!("Protocol {} is not supported.", prot).into())
    }
}

/// Open a string url to get a json struct
pub fn open_json_str<T: DeserializeOwned>(url: &str) -> Result<T> {
    let url = Url::parse(url)?;
    open_json(url)
}

#[test]
fn test_local() {
    use std::fs;
    let path = fs::canonicalize("./src/lib.rs").unwrap();
    let local_bytes = open_bytes(Url::parse(&format!("file://{}", path.display())).unwrap()).unwrap();
    assert!(local_bytes.len() > 0);

    let _ = open(Url::parse(&format!("file://{}", path.display())).unwrap()).unwrap();
}

#[test]
fn test_remote() {
    let remote_bytes = open_bytes(Url::parse("https://gitlab.com/Artefaritaj/file-fetcher/raw/master/src/lib.rs").unwrap()).unwrap();
    assert!(remote_bytes.len() > 0);

    let _ = open(Url::parse("https://gitlab.com/Artefaritaj/file-fetcher/raw/master/src/lib.rs").unwrap()).unwrap();
}
